package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import model.Dissenyador;
import principal.Component;
import vista.DissenyadorForm;
import vista.DissenyadorLlista;
import vista.MenuDissenyadorVista;

/**
 *
 * @author FTA
 */
public class ControladorDissenyador implements ActionListener {

    private MenuDissenyadorVista menuDissenyadorVista;
    private Dissenyador dissenyador = null;
    private DissenyadorForm dissenyadorForm = null;
    private DissenyadorLlista dissenyadorLlista = null;
    private int opcioSelec = 0;

    public ControladorDissenyador() {

        menuDissenyadorVista = new MenuDissenyadorVista();
        afegirListenersMenu();

    }

    private void afegirListenersMenu() {

        for (JButton unBoto : menuDissenyadorVista.getMenuButtons()) {
            unBoto.addActionListener(this);
        }

    }

    private void afegirListenersForm() {

        dissenyadorForm.getbDesar().addActionListener(this);
        dissenyadorForm.getbSortir().addActionListener(this);

    }

    private void afegirListenersLlista() {

        dissenyadorLlista.getbSortir().addActionListener(this);

    }

    @Override
    public void actionPerformed(ActionEvent e) {

        //Accions per al menú
        JButton[] elsBotons = menuDissenyadorVista.getMenuButtons();

        for (int i = 0; i < elsBotons.length; i++) {
            if (e.getSource() == elsBotons[i]) {
                menuDissenyadorVista.getFrame().setVisible(false);
                opcioSelec = i;
                bifurcaOpcio(i);
            }
        }

        //Accions per al formulari de dissenyadors
        if (dissenyadorForm != null) {

            if (e.getSource() == dissenyadorForm.getbDesar()) {

                if (opcioSelec == 1) {//Nou dissenyador

                    Dissenyador dissenyador = new Dissenyador(dissenyadorForm.gettNif().getText(), dissenyadorForm.gettNom().getText());
                    if (dissenyadorForm.gettActiu().isSelected()) {
                        dissenyador.setActiu(true);
                    } else {
                        dissenyador.setActiu(false);
                    }

                    ControladorPrincipal.getEstudiActual().getComponents().add(dissenyador);

                } else if (opcioSelec == 2) {//Modificar dissenyador

                    dissenyador.setNif(dissenyadorForm.gettNif().getText());
                    dissenyador.setNom(dissenyadorForm.gettNom().getText());
                    if (dissenyadorForm.gettActiu().isSelected()) {
                        dissenyador.setActiu(true);
                    } else {
                        dissenyador.setActiu(false);
                    }
                }

            } else if (e.getSource() == dissenyadorForm.getbSortir()) { //Sortir

                dissenyadorForm.getFrame().setVisible(false);
                menuDissenyadorVista.getFrame().setVisible(true);

            }

        }

        if (dissenyadorLlista != null) {

            if (e.getSource() == dissenyadorLlista.getbSortir()) {

                dissenyadorLlista.getFrame().setVisible(false);
                menuDissenyadorVista.getFrame().setVisible(true);

            }

        }

    }

    private void bifurcaOpcio(Integer opcio) {
        
        switch (opcio) {
            case 0: //sortir
                ControladorPrincipal.getMenuPrincipalVista().getFrame().setVisible(true);
                break;
            case 1: // alta
                dissenyadorForm = new DissenyadorForm();
                afegirListenersForm();
                break;
            case 2: // modificar
                int pos = ControladorPrincipal.getEstudiActual().selectComponent(1, seleccionarDissenyador());
                dissenyador = (Dissenyador) ControladorPrincipal.getEstudiActual().getComponents().get(pos);
                dissenyadorForm = new DissenyadorForm(dissenyador.getNif(), dissenyador.getNom(), dissenyador.getActiu());
                afegirListenersForm();
                break;
            case 3: // llista
                dissenyadorLlista = new DissenyadorLlista();
                afegirListenersLlista();
                break;
        }

    }

    private String seleccionarDissenyador() {
        
        int i = 0;
        
        for (Component component : ControladorPrincipal.getEstudiActual().getComponents()) {

            if (component instanceof Dissenyador) {
               i++;
            }

        }

        String[] nifsDissenyadors = new String[i];
        
        i=0;

        for (Component component : ControladorPrincipal.getEstudiActual().getComponents()) {

            if (component instanceof Dissenyador) {
                nifsDissenyadors[i] = ((Dissenyador) component).getNif();
            }
            
            i++;

        }

        int messageType = JOptionPane.QUESTION_MESSAGE;
        int code = JOptionPane.showOptionDialog(null, "Selecciona un dissenyador", "Selecció de dissenyadorss", 0, messageType, null, nifsDissenyadors, null);

        if (code != JOptionPane.CLOSED_OPTION) {
            return nifsDissenyadors[code];
        }

        return null;
    }

}
