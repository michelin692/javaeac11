package vista;

import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

/**
 *
 * @author FTA
 */
public class DissenyadorForm {

    private JFrame frame;

    private final int AMPLADA = 300;
    private final int ALCADA = 200;

    private JLabel lNif;
    private JTextField tNif;
    private JLabel lNom;
    private JTextField tNom;
    private JLabel lActiu;
    private JCheckBox tActiu;

    private JButton bDesar;
    private JButton bSortir;

    public DissenyadorForm() {

        //Definició de la finestra del menú
        frame = new JFrame("Formulari Dissenyador");
        frame.setLayout(new GridLayout(0, 1));

        //Creació dels controls del formulari
        lNif = new JLabel("Nif");
        tNif = new JTextField(8);
        lNom = new JLabel("Nom");
        tNom = new JTextField(20);
        lActiu = new JLabel("Actiu");
        tActiu = new JCheckBox();

        //Creació dels botons del formulari
        bDesar = new JButton("Desar");
        bSortir = new JButton("Sortir");

        //Addició del tot el formulari a la finestra
        frame.add(lNif);
        frame.add(tNif);
        frame.add(lNom);
        frame.add(tNom);
        frame.add(lActiu);
        frame.add(tActiu);
        frame.add(bDesar);
        frame.add(bSortir);

        //Es mostra la finestra amb propietats per defecte
        frame.setSize(AMPLADA, ALCADA);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }

    public DissenyadorForm(String nif, String nom, boolean actiu) {
        this();
        tNif.setText(nif);
        tNom.setText(nom);
        tActiu.setSelected(actiu);
    }

    public JFrame getFrame() {
        return frame;
    }

    public void setFrame(JFrame frame) {
        this.frame = frame;
    }

    public JTextField gettNif() {
        return tNif;
    }

    public void settNif(JTextField tNif) {
        this.tNif = tNif;
    }

    public JTextField gettNom() {
        return tNom;
    }

    public void settNom(JTextField tNom) {
        this.tNom = tNom;
    }

    public JCheckBox gettActiu() {
        return tActiu;
    }

    public void settActiu(JCheckBox tActiu) {
        this.tActiu = tActiu;
    }

    public JButton getbDesar() {
        return bDesar;
    }

    public void setbDesar(JButton bDesar) {
        this.bDesar = bDesar;
    }

    public JButton getbSortir() {
        return bSortir;
    }

    public void setbSortir(JButton bSortir) {
        this.bSortir = bSortir;
    }

}
