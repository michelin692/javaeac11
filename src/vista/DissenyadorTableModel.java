package vista;

import controlador.ControladorPrincipal;
import javax.swing.table.AbstractTableModel;
import model.Dissenyador;
import principal.Component;

/**
 *
 * @author FTA
 */
public class DissenyadorTableModel extends AbstractTableModel{
    
    private final String[] columnNames = {"NIF", "Nom", "Actiu"};

    String[][] data = new String[ControladorPrincipal.getMAXESTUDIS()][3];

    public DissenyadorTableModel() {
        int i = 0;
        for (Component component : ControladorPrincipal.getEstudiActual().getComponents()) {
            if (component instanceof Dissenyador) {
                data[i][0] = ((Dissenyador)component).getNif();
                data[i][1] = ((Dissenyador)component).getNom();
                data[i][2] = String.valueOf(((Dissenyador)component).getActiu());
                i++;
            }
        }
    }

    @Override
    public int getRowCount() {
        return data.length;
    }

    @Override
    public int getColumnCount() {
        return data[0].length;
    }

    @Override
    public String getColumnName(int col) {
        return columnNames[col];
    }

    @Override
    public Object getValueAt(int row, int column) {
        return data[row][column];
    }
    
}
