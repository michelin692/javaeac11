package persistencia;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import model.Dissenyador;
import model.Estudi;
import principal.GestorEstudisException;

/**
 *
 * @author fta
 */
public class GestorJDBC implements ProveedorPersistencia {

    private Estudi estudi;

    private Connection conn; //Connexió a la base de dades

    public Estudi getEstudi() {
        return estudi;
    }

    public void setEstudi(Estudi estudi) {
        this.estudi = estudi;
    }

    /*
     PreparedStatement necessaris
     */

 /*
     * TODO
     *
     * Obtenir un estudi
     * 
     * Sentència select de la taula estudis
     * Columnes: totes
     * Files: totes les que el codi d'estudi coincideixi amb el passat per paràmetre
     *
     */
    private static String codiEstudiSQL = "select * from estudis where codi = ?";

    private PreparedStatement codiEstudiSt;

    /*
     * TODO
     *
     * Inserir un estudi
     * 
     * Sentència d'inserció de la taula estudis
     * Els valors d'inserció són els que es donaran per paràmetre
     *
     */
    private static String insereixEstudiSQL = "insert into estudis ( codi , nom , adreca ) values (?,?,?)";
    
    private PreparedStatement insereixEstudiSt;

    /*
     * TODO
     *
     * Actualitzar un estudi
     * 
     * Sentència d'actualització de la taula estudis
     * Files a actualitzar: les que es corresponguin amb el codi passat per paràmetre
     * Columnes a actualitzar: nom i adreca amb els valors passat per paràmetre
     *
     */
    private static String actualitzaEstudiSQL = "update estudis set nom=? , adreca=? where codi = ?";

    private PreparedStatement actualitzaEstudiSt;

    /*
     * TODO
     *
     * Eliminar dissenyadors (donat el codi d'un estudi)
     * 
     * Sentència d'eliminació de la taula dissenyadors
     * Files a eliminar: les que es corresponguin amb el codi de l'estudi passat per paràmetre
     *
     */
    private static String eliminaDissenyadorSQL = "delete from dissenyadors where estudi = ?";

    private PreparedStatement eliminaDissenyadorSt;

    /*
     * TODO
     *
     * Inserir un dissenyador
     * 
     * Sentència d'inserció de la taula dissenyadors
     * Els valors d'inserció són els que es donaran per paràmetre
     *
     */
    private static String insereixDissenyadorSQL = "insert into dissenyadors (nif,nom,actiu,estudi) values (?,?,?,?)";

    private PreparedStatement insereixDissenyadorSt;

    /*
     * TODO
     *
     * Seleccionar els dissenyadors d'un estudi
     * 
     * Sentència select de la taula dissenyadors
     * Columnes: totes
     * Files: totes les que el codi d'estudi coincideixi amb el passat per paràmetre
     *
     */
    private static String selDissenyadorsSQL = "select * from dissenyadors where estudi=?";

    private PreparedStatement selDissenyadorsSt;

    /*
     *TODO
     * 
     *Paràmetres: cap
     *
     *Acció:
     *  - Heu d'establir la connexio JDBC amb la base de dades EAC111920S1
     *  - Heu de crear els objectes PrepareStatement declarats com a atributs d'aquesta classe
     *    amb els respectius SQL declarats com a atributs just sobre cadascun d'ells.
     *  - Heu de fer el catch de les possibles excepcions (en aquest mètode no llançarem GestorEstudisException,
     *    simplement, mostreu el missatge a consola de l'excepció capturada)
     *
     *Retorn: cap
     *
     */
    public void estableixConnexio() throws SQLException {
        try{
            //Carregar el controlador per la BD Apache Derby
            String urlBaseDades = "jdbc:derby://localhost:1527/EAC111920S1";
            String usuari = "root";
            String contrasenya = "root123";
            conn = DriverManager.getConnection(urlBaseDades , usuari, contrasenya);
            insereixEstudiSt = conn.prepareStatement(insereixEstudiSQL);
            codiEstudiSt = conn.prepareStatement(codiEstudiSQL);
            actualitzaEstudiSt = conn.prepareStatement(actualitzaEstudiSQL);
            insereixDissenyadorSt = conn.prepareStatement(insereixDissenyadorSQL);
            eliminaDissenyadorSt = conn.prepareStatement(eliminaDissenyadorSQL);
            selDissenyadorsSt = conn.prepareStatement(selDissenyadorsSQL);

        }catch( SQLException e ){
            System.out.println(e);
        }
    }

    public void tancaConnexio() throws SQLException {
        try {
            conn.close();
        } finally {
            conn = null;
        }
    }

    /*
     *TODO
     * 
     *Paràmetres: el nom del fitxer i l'estudi a desar
     *
     *Acció:
     *  - Heu de desar l'estudi sobre la base de dades:
     *  - L'estudi s'ha de desar a la taula estudis (nomFitxer és el codi de l'estudi)
     *  - Cada dissenyador de l'estudi, s'ha de desar com registre de la taula dissenyadors.
     *  - Heu de tenir en compte que si l'estudi ja existeix, heu de fer el següent:
     *     - Actualitzar el registre estudi ja existent
     *     - Eliminar tots els dissenyadors d'aquest estudi de la taula dissenyadors i després inserir els nous com si hagués estat
     *       un estudi nou.
     *  - Si al fer qualsevol operació es produeix una excepció, llavors heu de llançar l'excepció GestorEstudisException amb codi "GestorJDBC.desar"
     *
     *Retorn: cap
     *
     */
    @Override
    public void desarEstudi(String nomFitxer, Estudi estudi) throws GestorEstudisException {
        
        try{
            //estableix conexió
            if( conn == null ){
                estableixConnexio();
            }
            //consultamos si existe primero el estudio
            codiEstudiSt.setInt(1, Integer.parseInt(nomFitxer) );
            ResultSet res = codiEstudiSt.executeQuery();
            //si el estudio no existe
            if( !res.next() ){
                //insertamos el estudio
                insereixEstudiSt.setInt(1, Integer.parseInt(nomFitxer));
                insereixEstudiSt.setString(2, estudi.getNom());
                insereixEstudiSt.setString(3, estudi.getAdreca());
                int inserccio = insereixEstudiSt.executeUpdate();
                //insertamos dissenyadores si tiene.
                if( estudi.getComponents().size() > 0){
                    for(int i=0; i<estudi.getComponents().size();i++){
                        if( estudi.getComponents().get(i) instanceof Dissenyador ){
                            Dissenyador nouDissentador = (Dissenyador) estudi.getComponents().get(i);
                            //insertamos dissenyadores
                            insereixDissenyadorSt.setString(1, nouDissentador.getNif());
                            insereixDissenyadorSt.setString(2, nouDissentador.getNom());
                            insereixDissenyadorSt.setBoolean(3, nouDissentador.getActiu());
                            insereixDissenyadorSt.setInt(4, Integer.parseInt(nomFitxer));
                            int inserccio_dissenyadors = insereixDissenyadorSt.executeUpdate();
                        };
                    }
                }
            }else{
                //actualizamos el estudio ya que existe.
                actualitzaEstudiSt.setString(1, estudi.getNom());
                actualitzaEstudiSt.setString(2, estudi.getAdreca());
                actualitzaEstudiSt.setInt(3, Integer.parseInt(nomFitxer));
                int update = actualitzaEstudiSt.executeUpdate();
                //elimanos los posibles dissenyadors que hayan con ese código.
                eliminaDissenyadorSt.setInt(1, Integer.parseInt(nomFitxer));
                int delete_dissenyadors = eliminaDissenyadorSt.executeUpdate();
                //si tenemos dissenyadores los insertamos.
                if( estudi.getComponents().size() > 0){
                    for(int i=0; i<estudi.getComponents().size();i++){
                        if( estudi.getComponents().get(i) instanceof Dissenyador ){
                            Dissenyador nouDissentador = (Dissenyador) estudi.getComponents().get(i);
                            //insertamos dissenyadores
                            insereixDissenyadorSt.setString(1, nouDissentador.getNif());
                            insereixDissenyadorSt.setString(2, nouDissentador.getNom());
                            insereixDissenyadorSt.setBoolean(3, nouDissentador.getActiu());
                            insereixDissenyadorSt.setInt(4, Integer.parseInt(nomFitxer));
                            int inserccio_dissenyadors = insereixDissenyadorSt.executeUpdate();
                        };
                    }
                }
            }
            
           
        }catch( SQLException e ){
            System.out.println(e);
        }
    }

    /*
     *TODO
     * 
     *Paràmetres: el nom del fitxer de 'estudi
     *
     *Acció:
     *  - Heu de carregar l'estudi des de la base de dades (nomFitxer és el codi de l'estudi)
     *  - Per fer això, heu de cercar el registre estudi de la taula amb codi = nomFitxer
     *  - A més, heu d'afegir els dissenyadors a l'estudi a partir de la taula dissenyadors
     *  - Si al fer qualsevol operació es dona una excepció, llavors heu de llançar l'excepció GestorEstudisException 
     *    amb codi "GestorJDBC.carregar"
     *  - Si el nomFitxer donat no existeix a la taula estudis (és a dir, el codi = nomFitxer no existeix), llavors
     *    heu de llançar l'excepció GestorEstudisException amb codi "GestorJDBC.noexist"
     *
     *Retorn: cap
     *
     */
    @Override
    public void carregarEstudi(String nomFitxer) throws GestorEstudisException {
        
        try{
            //sino tenemos conexion la establecemos.    
            if( conn == null ){
                estableixConnexio();
            }
            
            //select para saber si existe el estudio.
            codiEstudiSt.setInt(1, Integer.parseInt(nomFitxer));
            ResultSet resEstudi = codiEstudiSt.executeQuery();
            //si existe estudio lo devolvemos.
            
            if( resEstudi.next() ){
                estudi = new Estudi(resEstudi.getInt("codi"),resEstudi.getString("nom"),resEstudi.getString("adreca"));
                
                //consultamos los disseynadores si tuviese.
                selDissenyadorsSt.setInt(1, Integer.parseInt(nomFitxer));
                ResultSet resDissenyador = selDissenyadorsSt.executeQuery();
                if( resDissenyador.next() ){
                    estudi.addDissenyador(new Dissenyador(resDissenyador.getString("nif"),resDissenyador.getString("nom"),resDissenyador.getBoolean("actiu")));
                }
                
            }else{
                throw new GestorEstudisException("GestorJDBC.noExisteix");
            }
           
            
        }catch( SQLException e ){
            //System.out.println(e);
        }
        
    }

}
