package persistencia;

import com.db4o.Db4oEmbedded;
import model.Estudi;
import principal.GestorEstudisException;

import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;

/**
 *
 * @author fta
 */
public class GestorDB4O implements ProveedorPersistencia {

    private ObjectContainer db;
    private Estudi estudi;

    public Estudi getEstudi() {
        return estudi;
    }

    public void setEstudi(Estudi estudi) {
        this.estudi = estudi;
    }

    /*
     *TODO
     * 
     *Paràmetres: cap
     *
     *Acció:
     *  - Heu de crear / obrir la base de dades "EAC111920S1.db4o"
     *  - Aquesta base de dades ha de permetre que Estudi s'actualitzi en cascada.
     *
     *Retorn: cap
     *
     */
    public void estableixConnexio() {
       db = Db4oEmbedded.openFile("EAC111920S1.db4o");
    }

    public void tancaConnexio() {
        db.close();
    }

    /*
     *TODO
     * 
     *Paràmetres: el nom del fitxer i l'estudi a desar
     *
     *Acció:
     *  - Heu d'establir la connexio i al final tancar-la.
     *  - Heu de desar l'objecte Estudi passat per paràmetre sobre la base de dades 
     *    (heu d'inserir si no existia ja a la base de dades, o actualitzar en altre cas)
     *  - S'ha de fer la consulta de l'existència amb Predicate
     *
     *Retorn: cap
     *
     */
    @Override
    public void desarEstudi(String nomFitxer, Estudi pEstudi) throws GestorEstudisException {
       if( db == null ){
           estableixConnexio();
       }
       try {
           //primero consultamos para saber si existe el estudio.
           Estudi EstudiConsultar = new Estudi(pEstudi.getCodi(),pEstudi.getNom(),pEstudi.getAdreca());
           ObjectSet<Estudi> resultEstudis = db.queryByExample(EstudiConsultar);
           if(resultEstudis.hasNext()){
               //actualizamos el estudio.
               Estudi estudiModificar = resultEstudis.next(); 
               estudiModificar.setCodi(pEstudi.getCodi());
               estudiModificar.setNom(pEstudi.getNom());
               estudiModificar.setAdreca(pEstudi.getAdreca());
               db.store(estudiModificar);
           }else{
               //creamos el estudio nuevo
                Estudi[] estudisNous = {
                    new Estudi(pEstudi.getCodi(),pEstudi.getNom(),pEstudi.getAdreca())
                };
                for(int i = 0; i < estudisNous.length; i++) {
                     db.store(estudisNous[i]);
                }
           }
           
            
        } finally {
           tancaConnexio();
        }
    }

    /*
     *TODO
     * 
     *Paràmetres: el nom del fitxer on està guardat l'estudi
     *
     *Acció:
     *  - Heu d'establir la connexio i al final tancar-la.
     *  - Heu de carregar l'estudi des de la base de dades assignant-lo a l'atribut estudi.
     *    Si no existeix, llanceu l'excepció GestorEstudisException amb codi "GestorDB4O.noExisteix"
     *  - S'ha de fer la consulta amb Predicate
     *
     *Retorn: cap
     *
     */
    @Override
    public void carregarEstudi(String nomFitxer) throws GestorEstudisException {
      
        if( db == null ){
           estableixConnexio();
       }
        try {
            Estudi Estudi1 = new Estudi(Integer.parseInt(nomFitxer),null,null);
            ObjectSet<Estudi> resultEstudis = db.queryByExample(Estudi1);
            if( resultEstudis.hasNext() ){
                while (resultEstudis.hasNext()) {
                    estudi = resultEstudis.next();
                }
            }else{
                throw new GestorEstudisException("GestorDB4O.noExisteix");
            }
        } finally {
            tancaConnexio();
        }
    
    }
}
